# Arch Linux Kurulum ve Kullanıcı Kılavuzu

### Çalışma sahibi ve Katkı sağlayan: Ramazan Altıntop <ramazanaltintop@protonmail.com>

### Katkı sağlayan: Demiray Muhterem - tulliana <tulliana@archman.org>

Bu kılavuz ile kolayca Arch linux kurabileceğiniz gibi birçok çözüm de elinizin altında olacak.

#### GÜNCELLEME: 4 ŞUBAT 2020 / 17:00

#### SÜRÜM : ARCH LINUX REHBERİ 5.0

Linkedin: https://www.linkedin.com/in/ramazanaltintop/

Facebook Sayfamız: https://www.facebook.com/ArchLinuxTR

Facebook Grubumuz: https://www.facebook.com/groups/archlinuxtr

Facebook Sayfamız: https://www.facebook.com/linuxpark

Gitlab: https://gitlab.com/Archman-OS/archlinux_kullanici_kilavuzu

Bu belge Ramazan ALTINTOP tarafından Arch Linux kullanıcıları için hazırlanmıştır.

İçeriğe olacak olan katkılarınızı gitlab adresimizden veya ramazanaltintop@protonmail.com adresine gönderebilirsiniz.

Belge ve içerikleri daima güncel tutulacaktır.

Belgeye katkı sağlayan kişilerin isimleri döküman sonunda "Katkı ve Gelişim" bölümünde yayınlanmaktadır.


