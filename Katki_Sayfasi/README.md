# Arch Linux Kılavuzu Katkı Sayfası

## LaTeX Nedir?

LaTeX bir döküman işaretleme dilidir. LaTeX, teknik veya bilimsel makaleler, sayfalar, raporlar, kitaplar ve diğer dökümanları oluşturmak için önerilir.

## LaTeX Nasıl Kurulur?

`$ sudo pacman -S texlive-most`

![LaTeX Kurulumu Nasıl Yapılır ?](images/1.png)

## Texmaker Nasıl Kurulur?

![Texmaker Kurulumu Nasıl Yapılır ?](images/2.png)

## Proje Nasıl İndirilir?

Aşağıdaki komutu kullanarak projeyi indirebilirsiniz.

`$ git clone https://gitlab.com/Archman-OS/archlinux_kullanici_kilavuzu.git`

![Proje Nasıl İndirilir ?](images/3.png)

## PDF'e Nasıl Ulaşabilirim ?

Proje dizininde `arch-linux-kilavuzu.pdf` dosyasından yapılan çalışmaya ulaşabilirsiniz.

![PDF'e Nasıl Ulaşabilirim ?](images/4.png)

## Projede değişiklikler yaptım. PDF olarak nasıl güncelleyebilirim ?

`arch-linux-kilavuzu.tex` dosyasını `Texmaker` ile açınız.

![PDF Nasıl Güncellenir ?](images/5.png)

Ok işaretine tıklayınız ve PDF'in tekrar oluşturulmasını bekleyiniz.

![PDF Nasıl Güncellenir ?](images/6.png)

PDF başarılı bir şekilde güncellendi.

![PDF Nasıl Güncellenir ?](images/7.png)